# pix2pixTutorial
In CVPR 2017, Isola et. al proposed pix2pix model based on Conditional GANs for image-to-image translation: [paper](chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://arxiv.org/pdf/1611.07004.pdf)

![](image_translation.png)

## Getting started

This tutorial is heavily inspired by the Github repo published by the original authors of the paper: [pytorch-CycleGAN-and-pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/tree/master)

This tutorial is a TLDR for the original repo, hoping to help you get the model installed and running as seamlessly as possible.

<details><summary>Install Anaconda [Optional]</summary>
Anaconda helps you create an environment that allows isolating dependencies from other environments as well easy dependency management. For more details, you can visit: [Anaconda website](https://docs.anaconda.com/). 
<p>&nbsp;</p>
In the following tutorial, we will see the steps to install Conda in Linux Terminal:

+ Get the link to the [Anaconda Installer](https://www.anaconda.com/download#downloads) as follows:
![](anaconda_installer.png)
+ Open the terminal and download the installer using `wget`
``` 
wget https://repo.anaconda.com/archive/Anaconda3-2023.07-2-Linux-x86_64.sh 
```

+ Following this, you can head over to the [Anaconda Installation Guide](https://docs.anaconda.com/free/anaconda/install/linux/#installation) and continue from Step 3

+ After Conda is installed, you can go ahead and create your new isolated Conda environment:
```
conda create --name snowflakes
#replace snowflakes with the name of your environment
```
+ Now activate the environment
```
conda activate snowflakes
#replace snowflakes with the name of your environment
```

</details>

### Download the project:
```
git clone https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix
cd pytorch-CycleGAN-and-pix2pix
```

### Install pre-requisites
Following are the pre-requirements to run the model:
```
torch>=1.4.0
torchvision>=0.5.0
dominate>=2.4.0
visdom>=0.1.8.8
wandb
```
1. Install `Pytorch` using the command as generated from the [Installation Website](https://pytorch.org/):
![](pytorch.png)
2. Install `Torchvision` (computer vision library)
```
#using pip
pip install torchvision
#using conda
conda install -c pytorch torchvision
```
3. Install `dominate`
```
#using pip
pip install dominate
#using conda
conda install -c conda-forge dominate
```
4. Install `visdom`
```
#using pip
pip install visdom
#using conda
conda install -c conda-forge visdom
```
5. Install `wandb`
```
#using pip
pip install wandb
#using conda
conda install -c conda-forge wandb
```
## Download dataset and try the model
In this section, we will download the datasets provided by the authors and then train-test the pix2pix model on them:
### Dataset
Authors provide the following datasets:
```
cityscapes
night2day
edges2handbags
edges2shoes
facades
maps
```
To download any one of them:
```
cd pytorch-CycleGAN-and-pix2pix
bash ./datasets/download_pix2pix_dataset.sh facades
```
<details><summary>Visdom to view training progress on GUI [Optional] </summary>
Run

```
python -m visdom.server
```
Then click on URL:  http://localhost:8097 to view the training progress.

</details>

### Train a model
```
python train.py --dataroot ./datasets/facades --name facades_pix2pix --model pix2pix --direction BtoA --gpu_ids -1 --batch_size 10 --n_epochs 100 --lr 0.001
```
`--dataroot` add the path to the dataset. Above command assumes the default location

`--name` name of the model after it trains completely and gets saved in your system

`--model` make sure it is set to `pix2pix` in order to use this model

`--direction` This can have two values: `AtoB` or `BtoA` depending on the direction of image translation

`--gpu_ids` -1 will train on CPU; use `--gpu_ids <GPU_ID>` to train in multi-GPU mode ( for e.g. `--gpu_ids 0, 1` to use GPUs 0 and 1)

`--batch_size` default value is 1

`--n_epochs` default value is 100; number of epochs with the initial learning rate

`--lr` default value is 0.0002; initial learning rate for adam

### Optional flags
In addition to the above useful flags, you can add the following flags based on your need.

`--display_id` Setting it to > 0 will plot the training loss on [visdom] (In case you followed the Visdom step earlier). Setting to -1 can save this overhead

`--no_html` By default, the code persists intermediate results `[opt.checkpoints_dir]/[opt.name]/web/]`. To save the overhead, this flag will disable this.
Here `opt.checkpoints_dir` and `opt.name` corresponding to the default values as mentioned in the code unless specified

You can get further details about the all the flags implemented by the authors here:
1. [Training/ Testing tips](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md)
2. [Options Python Files](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/tree/master/options) This folder contains the base options file and train/test options file. You can check the code to find futher options to help test/train.

### Test Model
```
python test.py --dataroot ./datasets/facades --name facades_pix2pix --model pix2pix --direction BtoA
```
Make sure the `--name` and `--dataroot` are persistent with the previous step

## Prepare custom dataset
The details for this step are mentioned by the authors [here](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md#prepare-your-own-datasets-for-pix2pix)

TLDR;

Pix2pix needs paired images: 
+ Source image: you want to translate from this
+ Target image: you want to translate to this

In order to use the code for the model seamlessly, you have to ensure the custom dataset follows a certain folder heirarchy.

```
/path/to/data/A
                /train
                /test
                /val
/path/to/data/B
                /train
                /test
                /val
```
You will have folders corresponding to source images and target images labelled as A and B ( or vice-versa).

Each folder should have subfolders to have training images (`/train`), validation images (`/val`), and test images (`/test`)

**Make sure that for an image pair {A, B}, their names are same in both A and B folders.**

**For example: `/path/to/data/A/train/1.jpg` is considered to correspond to `/path/to/data/B/train/1.jpg`**


### Use pre-trained models
The authors also provide pre-trained models for users to directly test them on their datasets: [here](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/tree/master#apply-a-pre-trained-model-pix2pix)

## Acknowledgment
All figures in this tutorial and steps are credited to the following authors:

Isola, Phillip, Jun-Yan Zhu, Tinghui Zhou, and Alexei A. Efros. "Image-to-image translation with conditional adversarial networks." In Proceedings of the IEEE conference on computer vision and pattern recognition, pp. 1125-1134. 2017.
